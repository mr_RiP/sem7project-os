/*++

Module Name:

    FileBlocker.c

Abstract:

    This is the main module of the FileBlocker miniFilter driver.

Environment:

    Kernel mode

--*/

#include <fltKernel.h>
#include <dontuse.h>
#include <suppress.h>
#include "FileNamesCollection.h"

#pragma prefast(disable:__WARNING_ENCODE_MEMBER_FUNCTION_POINTER, "Not valid for kernel mode drivers")

PFLT_FILTER gFilterHandle;
ULONG_PTR OperationStatusCtx = 1;

ULONG gTraceFlags = 0;

/*************************************************************************
	Global Variables
*************************************************************************/

// Collection of files (names) user decided to block form any change
FILENAME_COLLECTION blockedFiles =
{
	NULL,	// Collection
	0,		// Count
	0		// MaxCount
};

/*************************************************************************
    Prototypes
*************************************************************************/

EXTERN_C_START

DRIVER_INITIALIZE DriverEntry;
NTSTATUS
DriverEntry (
    _In_ PDRIVER_OBJECT DriverObject,
    _In_ PUNICODE_STRING RegistryPath
    );

NTSTATUS
FileBlockerInstanceSetup(
	_In_ PCFLT_RELATED_OBJECTS FltObjects,
	_In_ FLT_INSTANCE_SETUP_FLAGS Flags,
	_In_ DEVICE_TYPE VolumeDeviceType,
	_In_ FLT_FILESYSTEM_TYPE VolumeFilesystemType
);

NTSTATUS
FileBlockerUnload (
    _In_ FLT_FILTER_UNLOAD_FLAGS Flags
    );

FLT_PREOP_CALLBACK_STATUS
FileBlockerPreOperation (
    _Inout_ PFLT_CALLBACK_DATA Data,
    _In_ PCFLT_RELATED_OBJECTS FltObjects,
    _Flt_CompletionContext_Outptr_ PVOID *CompletionContext
    );

FLT_PREOP_CALLBACK_STATUS
FileBlockerCreatePreOperation(
	_Inout_ PFLT_CALLBACK_DATA Data,
	_In_ PCFLT_RELATED_OBJECTS FltObjects,
	_Flt_CompletionContext_Outptr_ PVOID *CompletionContext
);

EXTERN_C_END

//
//  Assign text sections for each routine.
//

#ifdef ALLOC_PRAGMA
#pragma alloc_text(INIT, DriverEntry)
#pragma alloc_text(PAGE, FileBlockerUnload)
#pragma alloc_text(PAGE, FileBlockerPreOperation)
#pragma alloc_text(PAGE, FileBlockerCreatePreOperation)
#endif

//
//  operation registration
//

CONST FLT_OPERATION_REGISTRATION Callbacks[] = {

    { 
		IRP_MJ_WRITE,
		0,
		FileBlockerPreOperation,
		NULL 
	},
    { 
		IRP_MJ_SET_INFORMATION,
		0,
		FileBlockerPreOperation,
		NULL 
	},
    { 
		IRP_MJ_CREATE,      
		0,
		FileBlockerCreatePreOperation,
		NULL 
	},
    { IRP_MJ_OPERATION_END }
};

//
//  This defines what we want to filter with FltMgr
//

CONST FLT_REGISTRATION FilterRegistration = {

    sizeof( FLT_REGISTRATION ),         //  Size
    FLT_REGISTRATION_VERSION,           //  Version
    0,									//  Flags

    NULL,				//  Context
    Callbacks,			//  Operation callbacks

    FileBlockerUnload,	//  MiniFilterUnload

	FileBlockerInstanceSetup,	//  InstanceSetup
    NULL,	//  InstanceQueryTeardown
    NULL,	//  InstanceTeardownStart
    NULL,	//  InstanceTeardownComplete

    NULL,	//  GenerateFileName
    NULL,	//  GenerateDestinationFileName
    NULL	//  NormalizeNameComponent

};

/*************************************************************************
    MiniFilter initialization and unload routines.
*************************************************************************/

NTSTATUS
DriverEntry (
    _In_ PDRIVER_OBJECT DriverObject,
    _In_ PUNICODE_STRING RegistryPath
)
{
    NTSTATUS status;

    UNREFERENCED_PARAMETER( RegistryPath );

	status = FltRegisterFilter(DriverObject, &FilterRegistration, &gFilterHandle);

    if (NT_SUCCESS( status ))
	{
		InitFilenameCollection(&blockedFiles);
		status = CreateFilenameCollection(&blockedFiles);
		status = FltFileNamesToObjectNames(gFilterHandle, &blockedFiles);

		if (NT_SUCCESS( status ))
			status = FltStartFiltering(gFilterHandle);

		if (!NT_SUCCESS( status ))
			FltUnregisterFilter(gFilterHandle);
    }

    return status;
}

NTSTATUS
FileBlockerUnload(
    _In_ FLT_FILTER_UNLOAD_FLAGS Flags
)
{
    UNREFERENCED_PARAMETER( Flags );

    FltUnregisterFilter( gFilterHandle );
	FreeFilenameCollection(&blockedFiles);

    return STATUS_SUCCESS;
}

NTSTATUS
FileBlockerInstanceSetup(
	_In_ PCFLT_RELATED_OBJECTS FltObjects,
	_In_ FLT_INSTANCE_SETUP_FLAGS Flags,
	_In_ DEVICE_TYPE VolumeDeviceType,
	_In_ FLT_FILESYSTEM_TYPE VolumeFilesystemType
)
{
	UNREFERENCED_PARAMETER(FltObjects);
	UNREFERENCED_PARAMETER(Flags);
	UNREFERENCED_PARAMETER(VolumeFilesystemType);

	if (VolumeDeviceType == FILE_DEVICE_NETWORK_FILE_SYSTEM)
		return STATUS_FLT_DO_NOT_ATTACH;
	else
		return STATUS_SUCCESS;
}

/*************************************************************************
    MiniFilter callback routines.
*************************************************************************/
FLT_PREOP_CALLBACK_STATUS
FileBlockerPreOperation(
    _Inout_ PFLT_CALLBACK_DATA Data,
    _In_ PCFLT_RELATED_OBJECTS FltObjects,
    _Flt_CompletionContext_Outptr_ PVOID *CompletionContext
)
{
    UNREFERENCED_PARAMETER( FltObjects );
    UNREFERENCED_PARAMETER( CompletionContext );

    NTSTATUS status;
	PFLT_FILE_NAME_INFORMATION info;

	status = FltGetFileNameInformation(Data, FLT_FILE_NAME_NORMALIZED, &info);

	if (NT_SUCCESS(status) && ContainsFilename(&blockedFiles, &(info->Name)))
	{		
		Data->IoStatus.Status = STATUS_ACCESS_DENIED;
		return FLT_PREOP_COMPLETE;
	}

	return FLT_PREOP_SUCCESS_NO_CALLBACK;
}

FLT_PREOP_CALLBACK_STATUS
FileBlockerCreatePreOperation(
	_Inout_ PFLT_CALLBACK_DATA Data,
	_In_ PCFLT_RELATED_OBJECTS FltObjects,
	_Flt_CompletionContext_Outptr_ PVOID *CompletionContext
)
{
	if (FlagOn(Data->Iopb->Parameters.Create.Options, FILE_DELETE_ON_CLOSE))
		return FileBlockerPreOperation(Data, FltObjects, CompletionContext);

	return FLT_PREOP_SUCCESS_NO_CALLBACK;
}