#include "FileNamesCollection.h"
#include "FileNamesCollectionInner.h"

/*************************************************************************
	Constant values definitions
*************************************************************************/

#define SOURCE_FILE_OBJECT_NAME L"\\SystemRoot\\FileBlocker\\Collection.txt"
#define WCHAR_BUFFER_COUNT 260
#define COLLECTION_INIT_COUNT 10
#define COLLECTION_REALLOC_MULTIPLIER 2
#define BUFFER_POOL_TAG 'ffuB'
#define REMAINS_POOL_TAG 'ameR'
#define COLLECTION_POOL_TAG 'lCNF'
#define FILENAME_BUFFER_POOL_TAG 'fBNF'
#define FILENAME_STRUCT_POOL_TAG 'tSNF'
#define DRIVE_LETTER_WCHAR_COUNT 2
#define DRIVE_LETTER_BYTE_LENGTH (sizeof(WCHAR) * DRIVE_LETTER_WCHAR_COUNT)

/*************************************************************************
	Header function implementations
*************************************************************************/

VOID InitFilenameCollection(
	_Out_ PFILENAME_COLLECTION collection
)
{
	collection->Collection = NULL;
	collection->Count = 0;
	collection->MaxCount = 0;
}

NTSTATUS CreateFilenameCollection(
	_Out_ PFILENAME_COLLECTION collection
)
{
	NTSTATUS status;
	HANDLE fileHandle;

	status = SetupFile(&fileHandle);
	if (NT_SUCCESS(status))
	{
		status = ParseFileNames(collection, fileHandle);
		ZwClose(fileHandle);

		if (!NT_SUCCESS(status))
			FreeFilenameCollection(collection);
	}

	return status;
}

VOID FreeFilenameCollection(
	_Inout_ PFILENAME_COLLECTION collection
)
{
	UINT32 i;

	if (collection->Collection != NULL)
	{
		for (i = 0; i < collection->Count; ++i)
			if (collection->Collection[i] != NULL)
			{
				if (collection->Collection[i]->Buffer != NULL)
					ExFreePoolWithTag(collection->Collection[i]->Buffer, FILENAME_BUFFER_POOL_TAG);
				ExFreePoolWithTag(collection->Collection[i], FILENAME_STRUCT_POOL_TAG);
			}

		ExFreePoolWithTag(collection->Collection, COLLECTION_POOL_TAG);
	}

	collection->Count = 0;
	collection->MaxCount = 0;
}

BOOLEAN ContainsFilename(
	_In_ PFILENAME_COLLECTION collection,
	_In_ PUNICODE_STRING filename
)
{
	USHORT count;
	BOOLEAN currentString;
	BOOLEAN contains;
	UINT32 i;
	USHORT j;

	contains = FALSE;
	for (i = 0; i < collection->Count && contains == FALSE; ++i)
	{
		if (collection->Collection[i]->Length == filename->Length)
		{
			currentString = TRUE;
			count = filename->Length / sizeof(WCHAR);

			for (j = 0; j < count && currentString == TRUE; ++j)
				currentString = collection->Collection[i]->Buffer[j] == filename->Buffer[j];

			contains = currentString;
		}
	}

	return contains;
}

NTSTATUS FltFileNamesToObjectNames(
	_In_ PFLT_FILTER gFilterHandle,
	_Inout_ PFILENAME_COLLECTION collection
)
{
	NTSTATUS status;
	UINT32 i;
	UNICODE_STRING driveLetter;
	PFLT_VOLUME volume;

	status = STATUS_SUCCESS;
	for (i = 0; i < collection->Count && NT_SUCCESS(status); ++i)
	{
		InitDriveLetter(&driveLetter, collection->Collection[i]);
		status = FltGetVolumeFromName(gFilterHandle, &driveLetter, &volume);
		if (NT_SUCCESS(status))
			status = SwitchFilenameToObject(collection->Collection[i], volume);
	}

	if (!NT_SUCCESS(status))
		FreeFilenameCollection(collection);

	return status;
}

/*************************************************************************
	Inner function implementations
*************************************************************************/

NTSTATUS SetupFile(
	_Out_ PHANDLE fileHandle
)
{
	UNICODE_STRING unicodeName;
	OBJECT_ATTRIBUTES objectAttributes;
	IO_STATUS_BLOCK ioStatusBlock;

	RtlInitUnicodeString(&unicodeName, SOURCE_FILE_OBJECT_NAME);

	InitializeObjectAttributes(
		&objectAttributes,
		&unicodeName,
		OBJ_CASE_INSENSITIVE | OBJ_KERNEL_HANDLE,
		NULL,
		NULL
	);

	if (KeGetCurrentIrql() != PASSIVE_LEVEL)
		return STATUS_INVALID_DEVICE_STATE;

	return ZwCreateFile(
		fileHandle,
		GENERIC_READ,
		&objectAttributes,
		&ioStatusBlock,
		NULL,
		FILE_ATTRIBUTE_NORMAL,
		0,
		FILE_OPEN,
		FILE_SYNCHRONOUS_IO_NONALERT,
		NULL,
		0
	);
}

NTSTATUS ParseFileNames(
	_Out_ PFILENAME_COLLECTION collection,
	_In_ HANDLE fileHandle
)
{
	NTSTATUS status;
	UNICODE_STRING buffer;
	UNICODE_STRING remains;

	RtlInitUnicodeString(&buffer, NULL);
	RtlInitUnicodeString(&remains, NULL);

	status = SetupBuffer(&buffer);

	if (NT_SUCCESS(status))
		status = InitReadFile(fileHandle, &buffer);

	while (NT_SUCCESS(status) && buffer.Length == buffer.MaximumLength)
	{
		status = ReadFile(fileHandle, &buffer);
		if (NT_SUCCESS(status))
			status = ParseBuffer(collection, &buffer, &remains);
	}

	FreeBuffer(&buffer);
	FreeRemains(&remains);
	return status;
}

NTSTATUS SetupBuffer(
	_Out_ PUNICODE_STRING buffer
)
{
	PWCH buf;
	USHORT bufSize;
	
	bufSize = sizeof(WCHAR) * WCHAR_BUFFER_COUNT;
	buf = (PWCH)ExAllocatePoolWithTag(PagedPool, bufSize, BUFFER_POOL_TAG);
	if (buf == NULL)
		return STATUS_NO_MEMORY;

	buffer->Buffer = buf;
	buffer->MaximumLength = bufSize;
	buffer->Length = bufSize;

	return STATUS_SUCCESS;
}

NTSTATUS ReadFile(
	_In_ HANDLE fileHandle,
	_Inout_ PUNICODE_STRING buffer
)
{
	NTSTATUS status;
	IO_STATUS_BLOCK ioStatusBlock;

	status = ZwReadFile(
		fileHandle,
		NULL,
		NULL,
		NULL,
		&ioStatusBlock,
		buffer->Buffer,
		buffer->MaximumLength,
		NULL,
		NULL
	);


	if (NT_SUCCESS(status))
		buffer->Length = (USHORT)ioStatusBlock.Information;

	return status;
}

NTSTATUS InitReadFile(
	_In_ HANDLE fileHandle,
	_Inout_ PUNICODE_STRING buffer
)
{
	IO_STATUS_BLOCK ioStatusBlock;

	return ZwReadFile(
		fileHandle,
		NULL,
		NULL,
		NULL,
		&ioStatusBlock,
		buffer->Buffer,
		sizeof(WCHAR),
		NULL,
		NULL
	);
}

void FreeBuffer(
	_Inout_ PUNICODE_STRING buffer
)
{
	if (buffer->Buffer != NULL)
	{
		ExFreePoolWithTag(buffer->Buffer, BUFFER_POOL_TAG);
		buffer->Buffer = NULL;
	}
	buffer->Length = 0;
	buffer->MaximumLength = 0;
}

void FreeRemains(
	_Inout_ PUNICODE_STRING remains
)
{
	if (remains->Buffer != NULL)
	{
		ExFreePoolWithTag(remains->Buffer, REMAINS_POOL_TAG);
		remains->Buffer = NULL;
	}
	remains->Length = 0;
	remains->MaximumLength = 0;
}

NTSTATUS ParseBuffer(
	_Inout_ PFILENAME_COLLECTION collection,
	_In_ PUNICODE_STRING buffer,
	_Inout_ PUNICODE_STRING remains
)
{
	NTSTATUS status;
	USHORT count;
	USHORT newFileIndex;
	UNICODE_STRING newFileName;
	USHORT i;

	status = STATUS_SUCCESS;
	count = buffer->Length / sizeof(WCHAR);
	newFileIndex = 0;
	RtlInitUnicodeString(&newFileName, NULL);
	for (i = 0; i < count && NT_SUCCESS(status); ++i)
		if (buffer->Buffer[i] == L'\n')
		{
			if (remains->Length > 0)
				status = CompileFilenameFromRemains(
					&newFileName, remains, buffer, i - 1);
			else
				status = CompileFilenameFromBuffer(
					&newFileName, buffer, newFileIndex, i - newFileIndex - 1);

			if (NT_SUCCESS(status))
				status = AddToCollection(collection, &newFileName);

			newFileIndex = i + 1;
		}

	if (NT_SUCCESS(status))
		SetupRemains(remains, buffer, newFileIndex);

	return status;
}

NTSTATUS CompileFilenameFromRemains(
	_Out_ PUNICODE_STRING fileName,
	_In_ PUNICODE_STRING remains,
	_In_ PUNICODE_STRING buffer,
	_In_ USHORT bufferPartCount
)
{
	USHORT length;
	PWCH string;
	USHORT remainsCount;
	USHORT i;

	length = remains->Length + (bufferPartCount * sizeof(WCHAR));
	string = (PWCH)ExAllocatePoolWithTag(PagedPool, length, FILENAME_BUFFER_POOL_TAG);
	if (string == NULL)
		return STATUS_NO_MEMORY;

	fileName->Buffer = string;
	fileName->MaximumLength = length;
	fileName->Length = length;
	
	remainsCount = remains->Length / sizeof(WCHAR);

	for (i = 0; i < remainsCount; ++i)
		fileName->Buffer[i] = remains->Buffer[i];

	for (i = 0; i < bufferPartCount; ++i)
		fileName->Buffer[i + remainsCount] = buffer->Buffer[i];

	return STATUS_SUCCESS;
}

NTSTATUS CompileFilenameFromBuffer(
	_Out_ PUNICODE_STRING fileName,
	_In_ PUNICODE_STRING buffer,
	_In_ USHORT beginIndex,
	_In_ USHORT count
)
{
	USHORT length;
	PWCH string;

	length = count * sizeof(WCHAR);
	string = (PWCH)ExAllocatePoolWithTag(PagedPool, length, FILENAME_BUFFER_POOL_TAG);
	if (string == NULL)
		return STATUS_NO_MEMORY;

	fileName->Buffer = string;
	fileName->MaximumLength = length;
	fileName->Length = length;

	for (USHORT i = 0; i < count; ++i)
		fileName->Buffer[i] = buffer->Buffer[beginIndex + i];

	return STATUS_SUCCESS;
}

NTSTATUS AllocCollection(
	_Inout_ PFILENAME_COLLECTION collection
)
{
	UINT32 newLength;
	PUNICODE_STRING *newCollection;
	UINT32 newCount;
	UINT32 i;

	if (collection->Collection == NULL)
	{
		newLength = COLLECTION_INIT_COUNT * sizeof(PUNICODE_STRING);
		newCollection = (PUNICODE_STRING*)ExAllocatePoolWithTag(
			PagedPool, newLength, COLLECTION_POOL_TAG);

		if (newCollection == NULL)
			return STATUS_NO_MEMORY;

		collection->Collection = newCollection;
		collection->MaxCount = COLLECTION_INIT_COUNT;
		collection->Count = 0;

		for (i = 0; i < collection->MaxCount; ++i)
			collection->Collection[i] = NULL;
	}
	else if (collection->Count == collection->MaxCount)
	{
		newCount = collection->MaxCount * COLLECTION_REALLOC_MULTIPLIER;
		newLength = newCount * sizeof(PUNICODE_STRING);
		newCollection = (PUNICODE_STRING*)ExAllocatePoolWithTag(
			PagedPool, newLength, COLLECTION_POOL_TAG);

		if (newCollection == NULL)
			return STATUS_NO_MEMORY;

		for (i = 0; i < collection->Count; ++i)
			newCollection[i] = collection->Collection[i];

		ExFreePoolWithTag(collection->Collection, COLLECTION_POOL_TAG);

		collection->Collection = newCollection;
		collection->MaxCount = newCount;

		for (i = collection->Count; i < collection->MaxCount; ++i)
			collection->Collection[i] = NULL;
	}

	return STATUS_SUCCESS;
}

NTSTATUS AddToCollection(
	_Inout_ PFILENAME_COLLECTION collection,
	_In_ PUNICODE_STRING newFileName
)
{
	NTSTATUS status;
	PUNICODE_STRING newCollectionItem;

	status = AllocCollection(collection);
	if (NT_SUCCESS(status))
	{
		newCollectionItem = (PUNICODE_STRING)ExAllocatePoolWithTag(PagedPool, sizeof(UNICODE_STRING), FILENAME_STRUCT_POOL_TAG);
		if (newCollectionItem == NULL)
			return STATUS_NO_MEMORY;

		newCollectionItem->Buffer = newFileName->Buffer;
		newCollectionItem->Length = newFileName->Length;
		newCollectionItem->MaximumLength = newFileName->MaximumLength;

		collection->Collection[collection->Count++] = newCollectionItem;
	}

	return status;
}

NTSTATUS SetupRemains(
	_Inout_ PUNICODE_STRING remains,
	_In_ PUNICODE_STRING buffer,
	_In_ USHORT newFileIndex
)
{
	USHORT count;
	USHORT length;
	USHORT i;

	count = (buffer->Length / sizeof(WCHAR)) - newFileIndex;
	length = count * sizeof(WCHAR);

	if (length == 0)
		remains->Length = length;
	else
	{
		if (length > remains->MaximumLength)
		{
			if (remains->Buffer != NULL)
				ExFreePoolWithTag(remains->Buffer, REMAINS_POOL_TAG);

			remains->Buffer = (PWCH)ExAllocatePoolWithTag(
				PagedPool, length, REMAINS_POOL_TAG);
			if (remains->Buffer == NULL)
				return STATUS_NO_MEMORY;

			remains->MaximumLength = length;
		}

		remains->Length = length;

		for (i = 0; i < count; ++i)
			remains->Buffer[i] = buffer->Buffer[newFileIndex + i];
	}

	return STATUS_SUCCESS;
}

VOID InitDriveLetter(
	_Out_ PUNICODE_STRING driveLetter,
	_In_ PUNICODE_STRING collectionFilename
)
{
	driveLetter->Buffer = collectionFilename->Buffer;
	driveLetter->MaximumLength = collectionFilename->MaximumLength;
	driveLetter->Length = DRIVE_LETTER_BYTE_LENGTH;
}

NTSTATUS SwitchFilenameToObject(
	_Inout_ PUNICODE_STRING collectionFilename,
	_In_ PFLT_VOLUME volume
)
{
	NTSTATUS status;
	ULONG volumeLength;
	UNICODE_STRING objectName;
	UINT32 i;
	USHORT pathCount;
	USHORT volumeCount;

	volumeLength = 0;

	status = FltGetVolumeName(volume, NULL, &volumeLength);
	if (NT_SUCCESS(status) || status == STATUS_BUFFER_TOO_SMALL)
	{
		objectName.Length = 0;
		objectName.MaximumLength = collectionFilename->MaximumLength + (USHORT)volumeLength - DRIVE_LETTER_BYTE_LENGTH;
		objectName.Buffer = (PWCH)ExAllocatePoolWithTag(PagedPool, objectName.MaximumLength, FILENAME_BUFFER_POOL_TAG);
		if (objectName.Buffer == NULL)
			return STATUS_NO_MEMORY;

		status = FltGetVolumeName(volume, &objectName, NULL);
		if (NT_SUCCESS(status))
		{
			volumeCount = objectName.Length / sizeof(WCHAR);
			pathCount = collectionFilename->Length / sizeof(WCHAR);
			for (i = DRIVE_LETTER_WCHAR_COUNT; i < pathCount; ++i)
				objectName.Buffer[i + volumeCount - DRIVE_LETTER_WCHAR_COUNT] = collectionFilename->Buffer[i];
			objectName.Length = objectName.MaximumLength;

			ExFreePoolWithTag(collectionFilename->Buffer, FILENAME_BUFFER_POOL_TAG);
			collectionFilename->Buffer = objectName.Buffer;
			collectionFilename->Length = objectName.Length;
			collectionFilename->MaximumLength = objectName.MaximumLength;
		}
		else
			ExFreePoolWithTag(objectName.Buffer, FILENAME_BUFFER_POOL_TAG);
	}

	return status;
}