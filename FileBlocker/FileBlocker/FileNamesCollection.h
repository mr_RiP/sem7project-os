#pragma once

#include <ntifs.h>
#include <fltKernel.h>

typedef struct _FILENAME_COLLECTION
{
	PUNICODE_STRING *Collection;
	UINT32 Count;
	UINT32 MaxCount;
} FILENAME_COLLECTION, *PFILENAME_COLLECTION;

// Initialize values
VOID InitFilenameCollection(
	_Out_ PFILENAME_COLLECTION collection
);

// Fill collection with file names from default source
NTSTATUS CreateFilenameCollection(
	_Out_ PFILENAME_COLLECTION collection
);

// Free allocated memory
VOID FreeFilenameCollection(
	_Inout_ PFILENAME_COLLECTION collection
);

// Check if collection contains selected file name
BOOLEAN ContainsFilename(
	_In_ PFILENAME_COLLECTION collection,
	_In_ PUNICODE_STRING filename
);

// Switch drive letters with volume names
NTSTATUS FltFileNamesToObjectNames(
	_In_ PFLT_FILTER gFilterHandle,
	_Inout_ PFILENAME_COLLECTION collection
);