#pragma once

#include "FileNamesCollection.h"

NTSTATUS SetupFile(
	_Out_ PHANDLE fileHandle
);

NTSTATUS ParseFileNames(
	_Out_ PFILENAME_COLLECTION collection,
	_In_ HANDLE fileHandle
);

NTSTATUS SetupBuffer(
	_Out_ PUNICODE_STRING buffer
);

NTSTATUS ReadFile(
	_In_ HANDLE fileHandle,
	_Inout_ PUNICODE_STRING buffer
);

NTSTATUS InitReadFile(
	_In_ HANDLE fileHandle,
	_Inout_ PUNICODE_STRING buffer
);

NTSTATUS ParseBuffer(
	_Inout_ PFILENAME_COLLECTION collection,
	_In_ PUNICODE_STRING buffer,
	_Inout_ PUNICODE_STRING remains
);

VOID FreeBuffer(
	_Inout_ PUNICODE_STRING buffer
);

VOID FreeRemains(
	_Inout_ PUNICODE_STRING remains
);

NTSTATUS CompileFilenameFromRemains(
	_Out_ PUNICODE_STRING fileName,
	_In_ PUNICODE_STRING remains,
	_In_ PUNICODE_STRING buffer,
	_In_ USHORT bufferPartCount
);

NTSTATUS CompileFilenameFromBuffer(
	_Out_ PUNICODE_STRING fileName,
	_In_ PUNICODE_STRING buffer,
	_In_ USHORT beginIndex,
	_In_ USHORT count
);

NTSTATUS AddToCollection(
	_Inout_ PFILENAME_COLLECTION collection,
	_In_ PUNICODE_STRING newFileName
);

NTSTATUS AllocCollection(
	_Inout_ PFILENAME_COLLECTION collection
);

NTSTATUS SetupRemains(
	_Inout_ PUNICODE_STRING remains,
	_In_ PUNICODE_STRING buffer,
	_In_ USHORT newFileIndex
);

VOID InitDriveLetter(
	_Out_ PUNICODE_STRING driveLetter,
	_In_ PUNICODE_STRING collectionFilename
);

NTSTATUS SwitchFilenameToObject(
	_Inout_ PUNICODE_STRING collectionFilename,
	_In_ PFLT_VOLUME volume
);