﻿using FileBlockerSDK;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileBlockerApplication
{
    public partial class MainForm : Form
    {
        private List<string> collection;

        public MainForm()
        {
            InitializeComponent();
            UpdateList();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            collection.Remove((string)fileNames.SelectedItem);
            fileNames.Items.RemoveAt(fileNames.SelectedIndex);
            if (fileNames.Items.Count == 0)
                btnDelete.Enabled = false;
            else
                fileNames.SelectedIndex = 0;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            UpdateList();
        }

        private void UpdateList()
        {
            fileNames.Items.Clear();
            collection = FileBlocker.GetCollection();
            if (collection.Any())
            {
                fileNames.Items.AddRange(collection.ToArray());
                fileNames.SelectedIndex = 0;
                btnDelete.Enabled = true;
            }
            else
                btnDelete.Enabled = false;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                collection = collection
                    .Union(openFileDialog.FileNames)
                    .OrderBy(str => str)
                    .ToList();

                string selected = (string)fileNames.SelectedItem;
                openFileDialog.FileName = null;
                fileNames.Items.Clear();
                fileNames.Items.AddRange(collection.ToArray());
                fileNames.SelectedItem = selected;

                if (collection.Any())
                    btnDelete.Enabled = true;
            }
        }

        private void btnReload_Click(object sender, EventArgs e)
        {
            try
            {
                FileBlocker.ResetCollection(collection);
                MessageBox.Show("Операция успешно выполнена!", "Успех", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception error)
            {
                string message = "В ходе выполнения операции произошла ошибка:\n" + error.Message;
                MessageBox.Show(message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void fileNames_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (fileNames.SelectedIndex >= 0)
            {
                string cmd = "explorer.exe";
                string arg = "/select, " + (string)fileNames.SelectedItem;
                Process.Start(cmd, arg);
            }
        }
    }
}
