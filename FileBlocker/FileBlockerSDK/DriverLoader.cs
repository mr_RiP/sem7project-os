﻿using System.Runtime.InteropServices;

namespace FileBlockerSDK
{
    internal static class DriverLoader
    {
        private const string driverServiceName = "FileBlocker";

        [DllImport("fltlib.dll", SetLastError = true)]
        private static extern uint FilterLoad([MarshalAs(UnmanagedType.LPWStr)]string strDriverName);

        [DllImport("fltlib.dll", SetLastError = true)]
        private static extern uint FilterUnload([MarshalAs(UnmanagedType.LPWStr)]string strDriverName);

        public static uint UnloadDriver()
        {
            return FilterUnload(driverServiceName);
        }

        public static uint LoadDriver()
        {
            return FilterLoad(driverServiceName);
        }
    }
}
