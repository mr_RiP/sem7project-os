﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace FileBlockerSDK
{
    public static class FileBlocker
    {
        public static string GetCollectionFilePath()
        {
            string rootPath = Environment.ExpandEnvironmentVariables("%systemroot%");
            string relativePath = "FileBlocker\\Collection.txt";
            return Path.Combine(rootPath, relativePath);
        }

        public static string GetCollectionDirectoryPath()
        {
            string rootPath = Environment.ExpandEnvironmentVariables("%systemroot%");
            string relativePath = "FileBlocker";
            return Path.Combine(rootPath, relativePath);
        }

        public static bool CollectionFileExists()
        {
            return File.Exists(GetCollectionFilePath());
        }

        public static void ResetCollection(List<string> fileNames)
        {

            string collectionFilePath = GetCollectionFilePath();
            var list = CreateWorkList(fileNames, collectionFilePath);

            UnloadDriver();
            using (var fileStream = CreateCollectionFile(collectionFilePath))
            using (var writer = new StreamWriter(fileStream, Encoding.Unicode))
            {
                foreach (var item in list)
                    writer.WriteLine(item);
            }
            LoadDriver();

        }

        public static void LoadDriver()
        {
            ProcessDriverAction(DriverLoader.LoadDriver);
        }

        public static void UnloadDriver()
        {
            ProcessDriverAction(DriverLoader.UnloadDriver);
        }

        public static List<string> GetCollection()
        {
            string collectionFile = GetCollectionFilePath();
            var collection = new List<string>();

            if (!File.Exists(collectionFile))
                return collection;

            using (var reader = new StreamReader(collectionFile, Encoding.Unicode))
            {
                while (!reader.EndOfStream)
                    collection.Add(reader.ReadLine());
            }

            collection.RemoveAll(str => string.IsNullOrEmpty(str) || str == collectionFile);
            return collection;
        }

        private static Stream CreateCollectionFile(string filePath)
        {
            if (!File.Exists(filePath))
            {
                string directoryPath = GetCollectionDirectoryPath();
                if (!Directory.Exists(directoryPath))
                    Directory.CreateDirectory(directoryPath);
            }

            return File.Create(filePath);
        }

        private static List<string> CreateWorkList(List<string> fileNames, string collectionFileName)
        {
            var list = new List<string>(fileNames);
            list.Add(collectionFileName);
            return list.Distinct().ToList();
        }

        private static void ErrorCheck(uint code)
        {
            if (code != 0)
                throw FileBlockerException.DriverLoadingError(code);
        }

        private static void ProcessDriverAction(Func<uint> action)
        {
            LoadDriverPriviledge.Enable();

            try
            {
                ErrorCheck(action());
            }
            finally
            {
                LoadDriverPriviledge.Disable();
            }
        }
    }
}
