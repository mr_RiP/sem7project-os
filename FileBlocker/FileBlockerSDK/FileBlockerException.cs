﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileBlockerSDK
{
    public class FileBlockerException : Exception
    {
        public FileBlockerException(int code) : base()
        {
            HResult = code;
        }

        public FileBlockerException(int code, string message) : base(message)
        {
            HResult = code;
        }

        public static Exception PrivilegeAjustmentError(int code)
        {
            return new FileBlockerException(code, "Privilege Ajustment Error: " + ErrorCodeMessage(code));
        }

        public static Exception DriverLoadingError(uint code)
        {
            string errorType = "Driver Loading Error: ";
            switch (code)
            {
                case 183:
                    return new FileBlockerException((int)code, errorType + "ERROR_ALREADY_EXISTS, " + ErrorCodeMessage((int)code));
                case 2:
                    return new FileBlockerException((int)code, errorType + "ERROR_FILE_NOT_FOUND, " + ErrorCodeMessage((int)code));
                case 1056:
                    return new FileBlockerException((int)code, errorType + "ERROR_SERVICE_ALREADY_RUNNING, " + ErrorCodeMessage((int)code));
                case 193:
                    return new FileBlockerException((int)code, errorType + "ERROR_BAD_EXE_FORMAT, " + ErrorCodeMessage((int)code));
                case 2001:
                    return new FileBlockerException((int)code, errorType + "ERROR_BAD_DRIVER, " + ErrorCodeMessage((int)code));
                case 577:
                    return new FileBlockerException((int)code, errorType + "ERROR_INVALID_IMAGE_HASH, " + ErrorCodeMessage((int)code));
                default:
                    return new FileBlockerException((int)code, errorType + ErrorCodeMessage((int)code));
            }
        }

        private static string ErrorCodeMessage(int code)
        {
            return "HResult = " + code + " (0x" + code.ToString("X") + ")";
        }

    }
}
