﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.ConstrainedExecution;

namespace FileBlockerSDK
{

    internal static class LoadDriverPriviledge
    {
        public static void Enable()
        {
            AjustPrivilege(true);
        }

        public static void Disable()
        {
            AjustPrivilege(false);
        }

        private static void AjustPrivilege(bool privilegeEnabled)
        {
            IntPtr processHandle = GetCurrentProcess();

            IntPtr tokenHandle = IntPtr.Zero;
            ErrorCheck(OpenProcessToken(processHandle, TokenAjustPrivileges | TokenQuery, ref tokenHandle));

            try
            {
                TOKEN_PRIVILEGES tokenPrivileges = InitializeTokenPrivileges(privilegeEnabled);
                ErrorCheck(LookupPrivilegeValue(null, SeLoadDriverNametext, ref tokenPrivileges.Luid));
                ErrorCheck(AdjustTokenPrivileges(tokenHandle, false, ref tokenPrivileges, 0, IntPtr.Zero, IntPtr.Zero));
            }
            finally
            {
                CloseHandle(tokenHandle);
            }
        }

        private static TOKEN_PRIVILEGES InitializeTokenPrivileges(bool privilegeEnabled)
        {
            TOKEN_PRIVILEGES tokenPrivileges;

            tokenPrivileges.PrivilegeCount = 1;
            tokenPrivileges.Luid.HighPart = 0;
            tokenPrivileges.Luid.LowPart = 0;
            tokenPrivileges.Attributes = privilegeEnabled ? PrivilegeEnabled : PrivilegeDisabled;

            return tokenPrivileges;
        }

        private static void ErrorCheck(bool returnValue)
        {
            if (!returnValue)
                throw FileBlockerException.PrivilegeAjustmentError(Marshal.GetLastWin32Error());
        }

        #region Native constants, structures and methods

        private const UInt32 PrivilegeDisabled = 0x00000000;
        private const UInt32 PrivilegeEnabled = 0x00000002;
        private const UInt32 TokenQuery = 0x00000008;
        private const UInt32 TokenAjustPrivileges = 0x00000020;
        private const string SeLoadDriverNametext = "SeLoadDriverPrivilege";

        [StructLayout(LayoutKind.Sequential)]
        private struct LUID
        {
            public Int32 LowPart;
            public UInt32 HighPart;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct TOKEN_PRIVILEGES
        {
            public UInt32 PrivilegeCount;
            public LUID Luid;
            public UInt32 Attributes;
        }

        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern Boolean CloseHandle(
            IntPtr hObject);

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern IntPtr GetCurrentProcess();

        [DllImport("advapi32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool OpenProcessToken(
            IntPtr processHandle,
            UInt32 desiredAccesss,
            ref IntPtr tokenHandle);

        [DllImport("advapi32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool LookupPrivilegeValue(
            string systemName,
            string name,
            [MarshalAs(UnmanagedType.Struct)] ref LUID luid);

        [DllImport("advapi32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool AdjustTokenPrivileges(
            IntPtr tokenHandle,
            [MarshalAs(UnmanagedType.Bool)] bool disableAllPrivileges,
            [MarshalAs(UnmanagedType.Struct)] ref TOKEN_PRIVILEGES newState,
            UInt32 zero, IntPtr null1, IntPtr null2);

        #endregion
    }
}