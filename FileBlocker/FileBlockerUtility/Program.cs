﻿using FileBlockerSDK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileBlockerUtility
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("FileBlocker Driver Loader Utility");
            Console.WriteLine();
            Console.WriteLine("Available commands:");
            Console.WriteLine("[Load] driver");
            Console.WriteLine("[Unload] driver");
            Console.WriteLine("[Exit] application");
            Console.WriteLine();

            string action = Console.ReadLine().ToLower();
            while (action != "exit")
            {
                try
                {
                    switch (action)
                    {
                        case "exit":
                            return;

                        case "load":
                            FileBlocker.LoadDriver();
                            Console.WriteLine("Success!");
                            break;

                        case "unload":
                            FileBlocker.UnloadDriver();
                            Console.WriteLine("Success!");
                            break;

                        default:
                            Console.WriteLine("Invalid command!");
                            break;
                    }
                }
                catch (Exception error)
                {
                    Console.WriteLine(error.Message);
                }

                Console.WriteLine();
                action = Console.ReadLine().ToLower();
            }
        }

        
    }
}
